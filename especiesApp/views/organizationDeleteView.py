from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from especiesApp.models.organizations import Organization
from especiesApp.serializers.organizationsSerializer import OrganizationsSerializer

class OrganizationsDeleteView(generics.DestroyAPIView):
    serializer_class = OrganizationsSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    lookup_field = "pk"