from rest_framework import serializers
from especiesApp.models.organizations import Organization

class OrganizationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ['id', 'name', 'city', 'telephone', 'address','url','description']