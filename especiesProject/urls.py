"""especiesProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from especiesApp import views


urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('admin/', admin.site.urls),
    path('user/', views.UserCreateView.as_view()),
    path('species/', views.SpeciesCreateView.as_view()),
    path('sighting/', views.SightingCreateView.as_view()),
    path('sighting/<int:fk>/', views.SightingGetView.as_view()),
    path('getSpecies/', views.SpeciesGetView.as_view()),
    path('regOrganizations/', views.OrganizationsCreateView.as_view()),
    path('getOrganizations/', views.OrganizationsGetView.as_view()),
    path('deleteOrganization/<int:pk>/', views.OrganizationsDeleteView.as_view()),
    path('updateSpecies/<int:pk>/', views.SpeciesAlterView.as_view())
]
