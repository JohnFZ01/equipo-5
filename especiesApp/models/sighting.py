from django.db import models
from .user import User
from .species import Species

class Sighting(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='sighting', on_delete=models.CASCADE)
    species = models.ForeignKey(Species, related_name='sighting', on_delete=models.CASCADE)
    latitude = models.FloatField()
    longitude = models.FloatField()
    location = models.TextField(max_length=400)
    health_state = models.CharField(choices=[('Bueno', 'Bueno'),
                                        ('Muy bueno', 'Muy bueno'),
                                        ('Regular', 'Regular'),
                                        ('Malo', 'Malo'),
                                        ('Muy malo', 'Muy malo'),]
                            ,default='Bueno'
                            ,max_length=10)
    quantity = models.IntegerField()
    comments = models.TextField(max_length=400)