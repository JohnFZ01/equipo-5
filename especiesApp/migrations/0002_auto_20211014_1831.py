# Generated by Django 3.2.8 on 2021-10-14 23:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('especiesApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='species',
            name='photo',
            field=models.TextField(default=None, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sighting',
            name='species',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sighting', to='especiesApp.species'),
        ),
    ]
