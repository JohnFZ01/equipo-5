from rest_framework import serializers
from especiesApp.models.sighting import Sighting

class SightingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sighting
        fields = ['id', 'latitude', 'longitude', 'location', 'health_state','quantity','species','user','comments']