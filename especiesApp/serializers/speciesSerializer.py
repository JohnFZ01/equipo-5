from rest_framework import serializers
from especiesApp.models.species import Species

class SpeciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Species
        fields = ['id', 'common_name', 'formal_name', 'family', 'description','photo']