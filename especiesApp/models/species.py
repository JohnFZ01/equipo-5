from django.db import models

class Species(models.Model):
    id = models.AutoField(primary_key=True)
    common_name = models.CharField(max_length=30)
    formal_name = models.CharField(max_length=100)
    family = models.CharField(max_length=50)
    description = models.TextField(max_length=400)
    photo = models.TextField(max_length=255)