from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from especiesApp.models.species import Species
from especiesApp.serializers.speciesSerializer import SpeciesSerializer

class SpeciesGetView(generics.ListAPIView):
    serializer_class = SpeciesSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Species.objects.all()