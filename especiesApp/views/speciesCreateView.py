from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from especiesApp.serializers.speciesSerializer import SpeciesSerializer

class SpeciesCreateView(generics.CreateAPIView):
    serializer_class = SpeciesSerializer
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        is_biologist = request.data.pop("isBiologist")
        if not is_biologist:
            return Response({"message":"No es biologo, no puede crear especies"}, status=status.HTTP_403_FORBIDDEN)
        return super.post(request, *args, **kwargs)
