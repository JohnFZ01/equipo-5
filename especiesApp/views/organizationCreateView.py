from rest_framework import status, views
from rest_framework.response import Response
from especiesApp.serializers.organizationsSerializer import OrganizationsSerializer

class OrganizationsCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = OrganizationsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("Organizacion creada con éxito", status=status.HTTP_201_CREATED)