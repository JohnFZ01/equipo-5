from django.apps import AppConfig


class EspeciesappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'especiesApp'
