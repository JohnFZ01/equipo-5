from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from especiesApp.models.organizations import Organization
from especiesApp.serializers.organizationsSerializer import OrganizationsSerializer

class OrganizationsGetView(generics.ListAPIView):
    serializer_class = OrganizationsSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()