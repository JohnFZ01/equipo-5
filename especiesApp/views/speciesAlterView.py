from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from especiesApp.models.species import Species
from especiesApp.serializers.speciesSerializer import SpeciesSerializer

class SpeciesAlterView(generics.UpdateAPIView):
    serializer_class = SpeciesSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Species.objects.all()
    lookup_field = "pk"
    def put(self, request, *args, **kwargs):
        is_biologist = request.data.pop("isBiologist")
        if not is_biologist:
            return Response({"message":"No es biologo, no puede modificar especies"}, status=status.HTTP_403_FORBIDDEN)
        return super().put(request, *args, **kwargs)