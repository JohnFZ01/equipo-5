from rest_framework import generics
from especiesApp.serializers.sightingSerializer import SightingSerializer
from rest_framework.permissions import IsAuthenticated

class SightingCreateView(generics.CreateAPIView):
    serializer_class = SightingSerializer
    permission_classes = (IsAuthenticated,)